# Notes

This repository gathers all my thoughts and tests on several tools I'm interested in. 

You might want to visit the Gitlab Page : https://alric_sans.gitlab.io/notes/

## Building locally 

- Windows :

```shell
virtualenv .env
./.env/Scripts/activate
pip install -r requirements.txt
mkdocs serve
```

## References 

- https://gitlab.com/pages/mkdocs
- https://squidfunk.github.io/mkdocs-material/