
## Design

| Context | Name | Url |
| ------- | --- | ---- | 
| Color palettes | Huemint | https://huemint.com/ |
|  | Coolors | https://coolors.co/ | 
|  | Material | https://material.io/resources/color/ | 
| Font pairing | Fontjoy | https://fontjoy.com/ | 
| Free icons | Flaticons | https://www.flaticon.com/ |

