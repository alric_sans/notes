# Gitlab introduction

## Roles

- *Project Manager*

Project Managers typically have the **Maintainer** or **Owner** roles within GitLab- meaning they can *add* or *delete members*, *establish permissions*, and *build out epics*, *milestones*, and other items necessary for Project Management.

- *Reporter*

Read-only contributors. They do not actively contribute to projects, but they view information for reporting and informational purposes.

- *Developer*

Direct contributors that can *read*, *write*, and *modify epics* and *issues* within GitLab projects and groups.

## Working with Issues

- Creating issues from templates
- Reviewing a MR
  - Creating suggestions / Applying suggestions

TODO : 
- integration with Source Graph & Prometheus

