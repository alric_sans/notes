# How to work with Gitlab

## Workflow 

1. Create an issue and explain the bug or feature you are willing to see implemented
2. Estimate the time to complete the issue, and set it in time tracking (no agile planning poker) 
3. Once you are ready to implement the feature or correct the bug, create the merge request and the branch
4. Dev, commit regularly and set regularly spent time on the issue (feel free to comment the issue regularly as well)

## Creating issues

- Always create issues from templates and follow the structure of the template (when possible).
- Always add an estimate of the time needed to complete the issue

## Time tracking

- Add an estimate (in the issue description or in a comment): 

```
\estimate 4h
```

- Add time spent (in a new comment): 

```
My little description of the time spent 
\spend 2h
```

## Time management

Export issues (you might want to filter issues properly before the export) from all the projects you worked on.

Merge all csv using pandas.

Make a summary of the issue to only keep interesting infos.
