## Overview

`Changelog` is just a file describing the features that a new release gathers. The entries that are present in a `Changelod` file can be generated automatically by Gitlab CI using the `changelog` API endpoint. The endpoint is able to : 
1. Generate the markdown syntax
2. Publish to the repo (with additional commit)

The thing is, we have to tell the endpoint, what the features are, and the way to tell it, is by using [Git trailers](https://git-scm.com/docs/git-interpret-trailers).

## Git trailers accepted 

| Value | Description |
| ----- | ----------- |
| `added` | New feature |
| `fixed` | Bug fix |
| `changed` | Feature change |
| `deprecated` | New deprecation |
| `removed` | Feature removal |
| `security` | Security fix |
| `performance` | Performance improvement |
| `other` | Other |

## Powershell API call exemple

```powershell
$headers = @{
     'PRIVATE-TOKEN' = 'glpat-DFypD2XCGVvvjry_qDrE' # Gitlab token
}
$body = @{
    version = '1.0.10' # Version displayed on the file
    from = '5c07ea055fcd469b06056632e6acc07c503c8d70' # Initial commit 
    to = 'd247718ad2715f19db93f2894fb00a849e7d921d' # Final commit 
    branch = 'main' # Branch to generate Changelog from
}
$url = "https://gitlab.com/api/v4/projects/43912284/repository/changelog" # Replace with the id of the project
Invoke-RestMethod -Uri $url -Method Post -Headers $headers -Body $body
```

## French config template 

To paste in a file in `.gitlab/changelog_config.yml`: 

```yml
---
categories:
  added: ':rocket: Nouvelle fonctionnalité'
  fixed: ':tools: Résolution de bug'
  changed: ':pencil2: Modification de fonctionnement'
  deprecated: ':timer: Déprécié'
  doc: ':page_facing_up: Documentation'
  removed: ':x: Supprimé'
  security: ':man_police_officer: Modification de sécurité'
  performance: ':fast_forward: Amélioration de performances'
  other: 'Autres'
include_groups:
  - poc98
date_format: '%d/%m/%y'
template: |
  {% if categories %}
  {% each categories %}
  ### {{ title }} ({% if single_change %}1 changement{% else %}{{ count }} changements{% end %})
  {% each entries %}
  - [{{ title }}]({{ commit.reference }})\
  {% if author %} by {{ author.reference }}{% end %}\
  {% if commit.trailers.MR %}\
   ([merge request]({{ commit.trailers.MR }}))\
  {% else %}\
  {% if merge_request %}\
   ([merge request]({{ merge_request.reference }}))\
  {% end %}\
  {% end %}\
  {% end %}
  {% end %}
  {% else %}
  No changes.
  {% end %}
```

Categories can be added, those are default but can be appended.

## References 
- https://docs.gitlab.com/ee/development/changelog.html
- https://docs.gitlab.com/ee/api/repositories.html#add-changelog-data-to-a-changelog-file