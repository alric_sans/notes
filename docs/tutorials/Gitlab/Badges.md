----
tags: [gitlab-badge, pipeline-status, latest-release]
----

## How to add a badge to a project

- Pipeline status

| Field | Value |
| ----- | ------ |
| Name | `pipelinestatus` |
| Link | https://gitlab.com/%{project_path}/ |
| Badge image url | https://gitlab.com/%{project_path}/badges/%{default_branch}/pipeline.svg |

- Latest release badge

| Field | Value |
| ----- | ------ |
| Name | `latestrelease` |
| Link | https://gitlab.com/%{project_path}/ |
| Badge image url | https://gitlab.com/%{project_path}/-/badges/release.svg?order_by=release_at |

## References

- https://docs.gitlab.com/ee/user/project/badges.html