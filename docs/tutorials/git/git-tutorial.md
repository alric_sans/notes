# How to

## Delete all local branches

It is impossible to remove a local branch if it still tracks a remote branch (although remotely deleted ...)

- List branches on local machine :

```shell
git branch -a
```

- **Show** branches to prune/cleanup the local references to remote branch

```shell
git remote prune origin --dry-run
```

- **Actually** Prune/Cleanup the local references to remote branch

```shell
git remote prune origin
```

- List branches on local machine

```shell
git branch -a
```

Now you can delete branches from Visual Studio or with : 

```shell
git branch -d my-branch-feature
```

### References 

https://www.fizerkhan.com/blog/posts/clean-up-your-local-branches-after-merge-and-delete-in-github

## Delete a commit that wasn't pushed

```shell
git reset HEAD~1
```

### References 

https://stackoverflow.com/questions/1611215/remove-a-git-commit-which-has-not-been-pushed

