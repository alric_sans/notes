
## Syntax

```powershell
$headers = @{
    'PRIVATE-TOKEN' = 'glpat-DFypD2XCGVvvjry_qDrE'
}
$body = @{
    version = '1.0.0'
    from = '5c07ea055fcd469b06056632e6acc07c503c8d70'
    to = 'fc6813357278c0e4389e1a01ed2aa05af9f58ab3'
    branch = 'main'
}
$url = "https://gitlab.com/api/v4/projects/43912284/repository/changelog"
Invoke-RestMethod -Uri $url -Method Post -Headers $headers -Body $body
```