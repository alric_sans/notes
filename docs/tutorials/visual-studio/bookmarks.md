# Bookmarks

| Action | Shortcut |
| -- | -- |
| Activate / Deactivate bookmark | ++ctrl+k++ , ++ctrl+k++ |
| New bookmark folder | ++ctrl+k++, ++ctrl+f++ |
| Previous bookmark | ++ctrl+k++, ++ctrl+p++ |
| Next bookmark | ++ctrl+k++, ++ctrl+n++ |
| Previous bookmark in folder | ++ctrl+shift+k++, ++ctrl+shift+p++ |
| Next bookmark in folder | ++ctrl+shift+k++, ++ctrl+shift+n++ |