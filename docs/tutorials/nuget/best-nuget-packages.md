| Context | Name | Url |
| ------- | --- | ---- | 
| Zip and archives | SharpZipLib | http://icsharpcode.github.io/SharpZip |
| Mails | FluentEmail | https://github.com/lukencode/FluentEmail |
|  | MailKit | https://github.com/jstedfast/MailKit |
|  | Papercut SMTP | https://github.com/ChangemakerStudios | 
| Microsoft Office | EPPlus | https://github.com/EPPlusSoftware/EPPlus | 
| Queues / Jobs | Hangfire | https://www.hangfire.io/ | 
|  | MassTransit | https://masstransit-project.com/ | 
|  | Polly | https://github.com/App-vNext/Polly | 
| Logs / Metrics | Serilog | https://github.com/serilog/serilog | 
|  | Seq | https://datalust.co/seq | 
|  | AppMetrics | https://github.com/AppMetrics/AppMetrics | 
| Benchmarks | Benchmark | https://benchmarkdotnet.org/index.html |
| Command Line Arguments | CommandLineUtils | https://github.com/natemcmaster/CommandLineUtils |
| Dependencies | Bullseye | https://github.com/adamralph/bullseye |