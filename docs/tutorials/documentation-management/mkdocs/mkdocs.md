
Customizing the theme (add `.css`, `.js`, override template) : 

https://squidfunk.github.io/mkdocs-material/customization/

Settings, plugins and extensions : 

https://squidfunk.github.io/mkdocs-material/setup/changing-the-colors/

**TODO**: add a default `mkdocs.yml` (and maybe `requirements.txt`) file for future use.