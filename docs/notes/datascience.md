# Datascience / ML

## Project template 

Best project template for python / ML projects : 

- Github : https://github.com/drivendata/cookiecutter-data-science

## Blog 

https://drivendata.co/blog.html

## Tools 

List of open source tools is available here : https://drivendata.co/open-source.html

### Deon 

Ethics checklist for ML models generator : 

https://deon.drivendata.org/

### Nbautoexport

Notebook export to `.py` on save :

https://github.com/drivendataorg/nbautoexport/

### ERD (Entity Relationship Diagrams)

https://erdantic.drivendata.org/stable/

### Competition winners 

https://github.com/drivendataorg/competition-winners

