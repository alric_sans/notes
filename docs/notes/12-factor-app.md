# 12 factor app

Official website : https://12factor.net/

## I. Codebase
One codebase tracked in revision control, many deploys

## II. Dependencies
Explicitly declare and isolate dependencies

## III. Config
Store config in the environment

## IV. Backing services
Treat backing services as attached resources

## V. Build, release, run
Strictly separate build and run stages

## VI. Processes
Execute the app as one or more stateless processes

## VII. Port binding
Export services via port binding

## VIII. Concurrency
Scale out via the process model

## IX. Disposability
Maximize robustness with **fast startup** and **graceful shutdown**.

- **Python**, manage *SIGTERM* to shutdown gracefully : 
  https://stackoverflow.com/questions/18499497/how-to-process-sigterm-signal-gracefully
- **AspNetCore**, manage graceful shutdown : 
  https://stackoverflow.com/questions/58422117/graceful-shutdown-asp-net-core

## X. Dev/prod parity
Keep development, staging, and production as similar as possible

## XI. Logs
Treat logs as event streams

## XII. Admin processes
Run admin/management tasks as one-off processes