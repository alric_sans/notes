# Python

## Packaging in 2023

https://drivendata.co/blog/python-packaging-2023

## Google docstrings 

https://github.com/google/styleguide/blob/gh-pages/pyguide.md#38-comments-and-docstrings

Typical example : 

```python
class SampleClass:
    """Summary of class here.

    Longer class information...
    Longer class information...

    Attributes:
        likes_spam: A boolean indicating if we like SPAM or not.
        eggs: An integer count of the eggs we have laid.
    """

    def __init__(self, likes_spam: bool = False):
        """Initializes the instance based on spam preference.

        Args:
            likes_spam: Defines if instance exhibits this preference.
        """
        self.likes_spam = likes_spam
        self.eggs = 0

    def public_method(self, hey: int) -> dict:
        """Performs operation blah.
        
        This is a longer description that can spread on several lines.

        Args:
            hey: Describes the arg
        
        Returns: 
            A dict mapping keys to the corresponding table row data
            fetched. Each row is represented as a tuple of strings. For
            example:

            {b'Serak': ('Rigel VII', 'Preparer'),
            b'Zim': ('Irk', 'Invader'),
            b'Lrrr': ('Omicron Persei 8', 'Emperor')}

            Returned keys are always bytes.  If a key from the keys argument is
            missing from the dictionary, then that row was not found in the
            table (and require_all_keys must have been False).
        
        Raises: 
            IOError: An error occurred accessing the smalltable.
        """
```

## Numpy docstrings
(uses restructured text docstrings under the hood https://docutils.sourceforge.io/rst.html)

https://numpydoc.readthedocs.io/en/latest/format.html#docstring-standard

Typical example : 

```python
class SampleClass:
    """Summary of class here.

    Longer class information...
    Longer class information...

    Attributes:
        likes_spam: A boolean indicating if we like SPAM or not.
        eggs: An integer count of the eggs we have laid.
    """

    def __init__(self, likes_spam: bool = False):
        """Initializes the instance based on spam preference.

        Parameters
        ----------
        likes_spam: bool
            Defines if instance exhibits this preference.
        """
        self.likes_spam = likes_spam
        self.eggs = 0

    def public_method(self, hey: int) -> dict:
        """Performs operation blah.
        
        This is a longer description that can spread on several lines.

        Parameters
        ----------
        hey: int
            Describes the arg
        
        Returns
        ------- 
        msg: dict
            A dict mapping keys to the corresponding table row data
            fetched. Each row is represented as a tuple of strings. For
            example:

            {b'Serak': ('Rigel VII', 'Preparer'),
            b'Zim': ('Irk', 'Invader'),
            b'Lrrr': ('Omicron Persei 8', 'Emperor')}

            Returned keys are always bytes.  If a key from the keys argument is
            missing from the dictionary, then that row was not found in the
            table (and require_all_keys must have been False).
        
        Raises
        ------ 
        IOError
            An error occurred accessing the smalltable.
        """
```


```