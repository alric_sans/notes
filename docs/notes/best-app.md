# Best app

My version of the 12-factor-app, implementing a subjective stack. 

## Version management

- Version should be bumped by the developper using :
    - **dotnet** : a dotnet tool, that edits the right `.csproj`
    - **python** : a python package, that edits the right `pyproject.toml`
- Version is read in CI and appended following the branches strategy

## Secret management

- Secrets should be stored in a vault (hashicorp, azure or equivalent), and retrieved by the application using client libraries

## Config management

- Config, for all stack should be managed using `.env` files
- All config files should be stored inside a `.config` folder at the root (whenever possible)
- Each environment should have its own `.env` file in the folder, for example : 

```tree
.config/
├─── local.env
├─── dev.env
├─── staging.env
└─── prod.env
```

- As secrets shall be managed in a vault, `.env` files should only contain urls, ports and app settings, thus can be pushed to the central repo
- Each time code is deployed (event in local env), make sure to copy the right `.env` file at the root (beware to not push this `.env` file to the repo): 

```shell
cp ./.config/local.env ./.env
```

- Loading variables can be done using those libraries : 
    - **dotnet** : https://github.com/bolorundurowb/dotenv.net
    - **python** : https://github.com/theskumar/python-dotenv

## Branches

- Projects should use the `Gitlab flow`, that is : 
    - `main` tracks the codebase
    - feature branches are created from main and merged in main
    - `prod` branch (protected) tracks code that is deployed in production environment
    - each time code is merged in `prod` branch, a release is created and code is deployed to production environment

## Log management

- Logs should use the standards depending on the stack
    - **dotnet**, use the `ILogger<T>` in AspNetCore
    - **python**, use the `logging` package 