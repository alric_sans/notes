# TODO 

## ASP.NET Core stack

- [ ] Default repo for a website 
- [ ] Default repo for a rabbitmq listener for the website 
- [ ] Default repo for a console app

## Python stack

- [ ] Default repo for a console app
- [ ] Default repo for an api triggering IA predictions
- [ ] Default repo for airflow dags for data cleaning