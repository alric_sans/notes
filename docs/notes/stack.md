# Stack for an ASP.net Core website

## Queing system

- RabbitMQ

## Secret management

- Vault

## Metrics

- AppMetrics to send from asp.netcore
- Prometheus

## Dashboarding 

- Grafana 

## Log management

- Serilog to send from asp.net core
- Seq

## Database

- PostgreSQL

## Load performance testing / stress testing

- k6 : https://code-maze.com/aspnetcore-performance-testing-with-k6/

## UnitTesting

- xUnit

## CI/CD

- build with fake database
- unit tests
- performance / stress 