# Versioning

## Semver 

> Semantic version numbers have 3 numeric components, Major.Minor.Patch. When you fix a bug, increment patch (1.0.0 → 1.0.1). When you release a new backwards-compatible feature, increment minor and reset patch to 0 (1.4.17 → 1.5.0). When you make a backwards-incompatible change, increment major and reset minor and patch to 0 (2.6.5 → 3.0.0)

## Solution by Visual Studio team : 

When merged in `main` :

`1.0.0-ci-20230407-100256`

When merged in `prod` : 

`1.0.0`

This means that for other `.csproj`, unable to merge on `prod` if one package has `-ci-...` as version



Source and articles : 

https://devblogs.microsoft.com/devops/versioning-nuget-packages-cd-1/
https://devblogs.microsoft.com/devops/versioning-nuget-packages-cd-2/
https://devblogs.microsoft.com/devops/versioning-nuget-packages-cd-3/

