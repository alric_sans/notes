# C#

## Package versioning (SemVer)

https://learn.microsoft.com/en-us/nuget/concepts/package-versioning

## Aspnet core Health Checks

https://learn.microsoft.com/en-us/aspnet/core/host-and-deploy/health-checks?view=aspnetcore-7.0

## Open telemetry / prometheus / Grafana

https://code-maze.com/tracking-dotnet-opentelemetry-metrics/
