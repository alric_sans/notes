# Notes

## Automatic Sem-Versioning

https://threedots.tech/post/automatic-semantic-versioning-in-gitlab-ci/

https://gitlab.com/threedotslabs/ci-scripts/blob/master/common/gen-semver

## Badges

https://github.com/badges/shields

## Best practices rules

- https://12factor.net/

## ASP.NET Core

- Global error handling middlewares
- Health checks for dashboarding per env

## Changelog

- Document git trailer format : https://docs.gitlab.com/ee/development/changelog.html
- Create a job that generates the changelog entries from the API : https://docs.gitlab.com/ee/api/repositories.html#generate-changelog-data

## Config management tools 

- Dotenv : https://github.com/motdotla/dotenv

## Deployments

- Gitlab CI artifacts for C# compiled binaries for deployment
- IIS auto deployment
- MSMQ auto deployment ? Rabbitmq auto deployment ?

## Gitlab 

- https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsaccessibility
- https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportscoverage_report
- https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportscodequality
- https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportscyclonedx
- https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsdotenv
- https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsjunit
- https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportssecret_detection

## Log management 

- Serilog enrichers
- AppMetrics

## VS Code

- Install extensions programatically
- Dev containers
- Jupyter slide show

## Git clone all hierarchy in Gitlab

- ghorg

## Commit messages

https://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html
