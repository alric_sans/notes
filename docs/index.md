
## Overview

This repo gathers all my thoughts and exploration about several tools that I'm interested in. 

This webpage got rendered using [Mkdocs](https://squidfunk.github.io/mkdocs-material/). 

## Prerequisites

- Python v3.8
- virtualenv (use `pip install virtualenv`)

## Setting up 

To set up this repo locally, you might want to use the following cpmmands : 

```shell
git clone git@gitlab.com:alric_sans/notes.git
cd ./notes
virtualenv .env
./.env/Scripts/activate
pip install -r requirements.txt
mkdocs serve
```

!!! note

    For a cheatsheet on Mkdocs markdown extensions and syntax, you might want to have a look [here](./tutorials/mkdocs/syntax.md).